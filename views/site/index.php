<?php

use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::$app->name ?></h1>
    </div>
    <div class="body-content">
      <div class="row">
        <div class="panel panel-primary">
  <!-- Default panel contents -->
         <div class="panel-heading">Listado de Noticias</div>
        <ul class="list-group">
         <?php
            foreach($datos as $registro){
                echo "<li class='list-group-item'>";
                echo yii\helpers\Html::a ($registro->titulo,['site/relacionadas','cat'=>$registro->categoria]);
                echo "</li>";
            }
        ?>
        </ul>
        </div>
     </div>
      
        <div class="row">
            <?php
            if(isset($categoria)){
                $categoria=strtoupper($categoria);
                echo GridView::widget([
                    'dataProvider'=>$dataprovider,
                    'summary'=>"<h4>Noticias Relacionadas</h4><h3>$categoria</h3>",
                    'columns'=>[
                    'id',
                    'titulo',
                    'texto',
                   ],
                ]);
            }
            ?>
        </div>
    </div>
</div>
